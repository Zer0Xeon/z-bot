const u = require('unirest');
const cjs = require('comfy.js');
const jp = require('jsonpath');
const owm = require('./api/openweathermap.json');
const tl = require('./api/twitch-login.json');
const debug = true;
const log = true;
const weather = `https://api.openweathermap.org/data/2.5/onecall?lat=${owm.lat}&lon=${owm.lon}&exclude=minutely,hourly,daily&appid=${owm.key}&units=metric`;
const dadjokes='https://icanhazdadjoke.com/';
const cn='https://api.icndb.com/jokes/random?exclude=[explicit]';
const version = 1.0;

console.log(`Starting Z-Bot v${version}`);
//Version Checking
u.get('https://zer0xeon.xyz/z-bot-version.json').end(function(result){
    if(jp.query(result.body, '$.version') == version){
        console.log('You are running the latest version!');
    }
    if(jp.query(result.body, '$.version') > version){
        console.log('There is an update for Z-Bot, go check it out at URL-TO-BE-ADDED');
    }
});
//Initialise ComfyJS (Thank you Instafluff)
cjs.Init(tl.user, tl.key);

console.log(`Logged into twitch chat as: ${tl.user}`)

//For errors on connection
cjs.onError = (error) => {
    console.log(error);
};


// Commands
cjs.onCommand = (user, command, message, flags, self, extra) => {
    if (flags.broadcaster && command === 'ping' && debug == true) {
        cjs.Say('Pong');
    }
    if ((flags.broadcaster || flags.mod) && (command == 'so' || command == 'shoutout') && message !== undefined) {
        cjs.Say(`Hey go give ${message} a look, they seem pretty cool.`);
        cjs.Say(`twitch.tv/${message}`);
        if (log == true) {
            console.log(`[Console] User: ${user} used Shoutout for ${message}.`);
        };
    }
    if(command == 'lurk'){
        cjs.Say(`${user} is lurking in the shadows waiting for their time to strike.`);
        if(log == true){
            console.log(`[Console] User ${user} used lurk`);
        }
    }
    if(command=='weather'){
        u.post(weather).end(function(result){
            let currentWeather = result.body;
            currentWeather = jp.query(currentWeather,'$.current.weather.0.main');
            cjs.Say("Zer0's Current weather is: " + currentWeather.toString() + " With a temperature of: " + jp.query(result.body, '$.current.temp')+"°C");
        })
    }
    if(command=='dadjoke'){
        u.get(dadjokes)
        .header("Accept", "text/plain")
        .header("User-Agent", `Z-Bot Version ${version} (https://gitlab.com/Zer0Xeon/z-bot)`)
        .end(function(result){
            cjs.Say(result.body);
        })
    }
    if(command=='chucknorris'){
        u.get(cn).end(function(result){
            let id = jp.query(result.body, '$.value.id');
            let quote = jp.query(result.body, '$.value.joke');
            cjs.Say(`#${id}: ${quote}`);
        })
    }
    if(command=='help'){
        if(flags.broadcaster || flags.mod){
            cjs.Say('Currently avalible commands are: !lurk, !weather, !so, !shoutout, !dadjoke, !chucknorris');
        }else{
            cjs.Say('Currently avalible commands are: !lurk, !weather, !dadjoke, !chucknorris');
        }
        
    }
}
cjs.onChat = (user,message)=>{
    console.log(`${user}: ${message}`);
}